use std::cmp::Ordering::{self, *};

pub trait QSortExt {
    type Item;

    fn qsort_by<Cmp>(&mut self, cmp: Cmp)
    where Cmp: Copy + Send + Fn(&Self::Item, &Self::Item) -> Ordering;

    fn qsort(&mut self) where Self::Item: Ord {
        self.qsort_by(Ord::cmp)
    }
}

impl<T: Send> QSortExt for [T] {
    type Item = T;
    
    fn qsort_by<Cmp>(&mut self, cmp: Cmp)
    where Cmp: Copy + Send + Fn(&T, &T) -> Ordering
    {
        // zero or one item is sorted
        if self.len() < 2 { return; }

        // two items just reverse
        if self.len() == 2 {
            let left_is = cmp(
                unsafe { self.get_unchecked(0) },
                unsafe { self.get_unchecked(1) },
            );

            if left_is == Greater { self.reverse() }
            return;
        }

        self.swap(self.len() - 1, self.len() / 2); // make midpoint last
        let pivot = {
            let (pivot, items) = self.split_last_mut().unwrap();
            let mut bottom = 0;
            let mut top = items.len() - 1;

            'partition: while top > bottom {
                while unsafe { cmp(items.get_unchecked(bottom), pivot) } != Greater {
                    bottom += 1;
                    if top <= bottom { break 'partition; }
                }

                while unsafe { cmp(items.get_unchecked(top), pivot) } != Less {
                    top -= 1;
                    if top <= bottom { break 'partition; }
                }

                items.swap(bottom, top);
                bottom += 1;
                top -= 1;
            }

            match cmp(&items[bottom], pivot) {
                Greater |
                Equal => bottom,
                Less  => bottom + 1,
            }
        };

        self.swap(self.len() - 1, pivot); // reset midpoint

        let (low, high) = self.split_at_mut(pivot);
        rayon::join(
            move || low .qsort_by(cmp),
            move || high.qsort_by(cmp),
        );
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::random;
    use std::iter::repeat_with;

    #[inline(never)]
    fn black_box<T>(x: T) -> T { x }

    #[test]
    fn empty() {
        let empty: &[i32] = &[];
        let mut vec = empty.to_vec();
        vec.qsort();
        assert_eq!(empty, vec.as_slice());
    }

    #[test]
    fn single() {
        let single = &[1i32];
        let mut vec = single.to_vec();
        vec.qsort();
        assert_eq!(single, vec.as_slice());
    }

    #[test]
    fn double() {
        let double = &[1, 2];
        let mut vec = double.to_vec();
        vec.qsort();
        assert_eq!(double, vec.as_slice());
        vec.reverse();
        vec.qsort();
        assert_eq!(double, vec.as_slice());
    }
    
    #[test]
    fn sorted() {
        let sorted = &[1,2,3,4,5];
        let mut vec = sorted.to_vec();
        vec.qsort();
        assert_eq!(sorted, vec.as_slice());
    }

    #[test]
    fn reversed() {
        let sorted = &[1,2,3,4,5];
        let mut vec = sorted.to_vec();
        vec.reverse();
        vec.qsort();
        assert_eq!(sorted, vec.as_slice());
    }

    #[test]
    fn small_fixed_random() {
        let sorted = &[1,2,3,4,5,6,7,8];
        let mut vec = vec![8,3,4,2,1,6,5,7];
        vec.qsort();
        assert_eq!(sorted, vec.as_slice());
    }

    #[test]
    fn med_random() {
        for _ in 0..10 {
            let mut reference: Vec<i32> = repeat_with(random).take(50).collect();
            let mut vec = reference.clone();
            reference.sort();
            vec.qsort();
            assert_eq!(reference, vec);
        }
    }

    #[test]
    fn big_random() {
        for _ in 0..10 {
            let mut reference: Vec<i32> = repeat_with(random).take(500000).collect();
            let mut vec = reference.clone();
            reference.sort();
            vec.qsort();
            assert_eq!(reference, vec);
        }
    }

    #[test]
    fn ord_pure_closure() {
        let reference: Vec<&str> = vec!["a", "bb", "aaa", "bbbb", "aaaaa", "bbbbbb"];
        let mut vec = vec!["bbbb", "a", "aaa", "bb", "bbbbbb", "aaaaa"];
        vec.qsort_by(|&lhs, &rhs| lhs.len().cmp(&rhs.len()));
        assert_eq!(reference, vec);
    }

    #[test]
    fn ord_capturing_closure() {
        let n: usize = black_box(1);
        let reference: Vec<&str> = vec!["aaa", "aaaaa", "baa", "aba", "bbbb", "bbbbbb"];
        let mut vec = vec!["bbbb", "aaa", "aba", "baa", "bbbbbb", "aaaaa"];
        vec.qsort_by(|&lhs, &rhs| {
            let l = lhs.chars().nth(n).unwrap();
            let r = rhs.chars().nth(n).unwrap();
            l.cmp(&r).then(lhs.cmp(&rhs))
        });
        assert_eq!(reference, vec);
    }
}
